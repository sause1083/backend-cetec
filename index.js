//Importamos las configuraciones de la carpeta config.

require('./config/config');
require('./models/db');
require('./config/passportConfig');


//Declaramos las variables objetos que traeran los datos de las clases
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const router = express.Router();

const operacionesUser = require('./routes/user.route');


const app = express();

app.use(bodyParser.json());
app.use(cors());


// Passport Middleware
app.use(passport.initialize());

//Rutas en las cuales ofrecemos los servicios

/* Invoca las rutas */


app.use('/', operacionesUser );

// Manejador de errores
app.use((err, req, res, next) => {
    if (err.name === 'ValidationError') {
        var valErrors = [];
        Object.keys(err.errors).forEach(key => valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors);
    }
});

//Inicialización del servicio
app.listen(process.env.PORT, () => {
    console.log(`Servidor iniciado en el puerto ${process.env.PORT}`);
});

