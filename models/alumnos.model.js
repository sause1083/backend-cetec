const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

let fecha = new Date(Date.now());

let alumnoSchema = new mongoose.Schema({
    numero_credencial: {
        type: String,
        required: true,
        unique: true
    },
    nombre:{
        type: String,
        required: true
    },
    apellido_paterno:{
        type: String,
        required: true
    },
    apellido_materno:{
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: fecha.getDay() + '/'+ fecha.getUTCMonth()+ '/'+ fecha.getUTCFullYear()
    },
    
    
});