const Alumno = require('../models/alumnos.model');

exports.registrarSocio = async (req, res) => {
    try {
       
        // Asignación de valores al socio
        let alumno;
        alumno = new Alumno(req.body);

        await alumno.save();
        res.send(alumno);
        
    } catch (error) {
        console.log(error);
        res.status(500).send("Ha ocurrido un error");
    }
}

